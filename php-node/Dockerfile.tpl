FROM webdevops/php-apache-dev:{PHP_VERSION}

RUN curl -fsSL https://deb.nodesource.com/setup_{NODE_VERSION}.x | bash - && \
    apt-get install -y nodejs && \
    docker-run-bootstrap && \
    docker-image-cleanup

RUN mkdir /data && chown -R 1000:1000 /data

EXPOSE 3000