#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import io, os, sys
import yaml

# prevent bytecode
sys.dont_write_bytecode = True

# Open stdout and stderr in binary mode, then wrap it in a TextIOWrapper and 
# enable write_through
try:
    # In case you are in Python 3 or above
    sys.stdout = io.TextIOWrapper(open(sys.stdout.fileno(), 'wb', 0), write_through=True)
    sys.stderr = io.TextIOWrapper(open(sys.stderr.fileno(), 'wb', 0), write_through=True)
except TypeError:
    # In case you are in Python 2 or below
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
    sys.stderr = os.fdopen(sys.stdout.fileno(), 'w', 0)

with open(r'images.yaml') as file:
    source = yaml.full_load(file)

    for params in source['images']:
        fin = open('Dockerfile.tpl', 'rt')
        content = fin.read()

        for attr, val in params.items():
            content = content.replace('{' + str(attr).upper() + '_VERSION}', str(val))

        fout = open('Dockerfile', 'w+')
        fout.write(content)
        fout.close()

        imageName = 'ardiefernandes/php-node:' + str(params['php']) + '-' + str(params['node'])
        
        os.system('docker build -t ' + imageName + ' --label repository=ardiefernandes/php-node .')
        os.system('docker push ' + imageName)
        os.system('docker image remove ' + imageName)

    os.system('docker build -t ardiefernandes/php-node:latest --label repository=ardiefernandes/php-node .')
    os.remove('Dockerfile')
    os.system('docker push ardiefernandes/php-node:latest')
    os.system('docker image remove ardiefernandes/php-node:latest')
    os.system('docker system prune -f')