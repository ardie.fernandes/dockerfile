FROM ardiefernandes/php-node:{PHP_VERSION}-{NODE_VERSION}

ENV LARAVEL_APP_ROOT /app
ENV LARAVEL_WORKER_DRIVER redis

COPY {WORKER_CONF_FILE} /opt/docker/etc/supervisor.d/laravel-worker.conf
COPY laravel-cron /opt/docker/etc/cron/laravel-cron
COPY 20-laravel-artisan.sh /opt/docker/provision/entrypoint.d/20-laravel-artisan.sh

EXPOSE 3001