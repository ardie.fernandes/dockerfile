# Replace markers
go-replace \
    -s "<LARAVEL_APP_ROOT>" -r "$LARAVEL_APP_ROOT" \
    -s "<LARAVEL_WORKER_DRIVER>" -r "$LARAVEL_WORKER_DRIVER" \
    -s "<APPLICATION_USER>" -r "$APPLICATION_USER" \
    --path=/opt/docker/etc/supervisor.d/laravel-worker.conf

go-replace \
    -s "<LARAVEL_APP_ROOT>" -r "$LARAVEL_APP_ROOT" \
    -s "<APPLICATION_USER>" -r "$APPLICATION_USER" \
    --path=/opt/docker/etc/cron/laravel-cron